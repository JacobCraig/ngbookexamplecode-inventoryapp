import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { Product } from '../product.model';

@Component({
  selector: 'products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  @Input() productList: Product[];
  @Output() onProductSelected: EventEmitter<Product>;

  private selectedProduct: Product;

  constructor() {
    this.onProductSelected = new EventEmitter<Product>();
  }

  public clicked = (product: Product) => {
    this.selectedProduct = product;
    this.onProductSelected.emit(this.selectedProduct);
  }

  public isSelected = (product: Product) => {
    if (product == null || this.selectedProduct == null) return false;
    return product.sku === this.selectedProduct.sku;
  }

  ngOnInit() {
  }
}
